#Ejercicio_2
#Funciones
import os
def menu():
    print('a_Cargar productos.')
    print('b_Mostrar productos.')
    print('c_Mostrar productos en un intervalo dado de stock.')
    print('d_Sumar X cantidad a un producto de stock Y')
    print('e_Eliminar productos con stock igual a cero.')
    print('f_Salir.')
    x = input('Ingrese una opcion: ')
    return x
def ingDescripcion():
    descripcion =  input('Descripción: ')
    return descripcion
def validarNum(mensaje):
    band = False
    while (band == False):
        num = input(mensaje)
        if (num.isnumeric()):
            num = int(num)
            if num >= 0:
                band = True
        else:
            print('Ingrese un numero positivo...')
    return num
def validarNum2(mensaje):
    band = False
    while (band == False):
        num = input(mensaje)
        if (num.isnumeric()):
            num = int(num)
            if num > 0:
                band = True
            else:
                print('Ingrese un precio correcto...')
        else:
            print('Ingrese un precio correcto...')
    return num
def cargarProductos():
    productos = {}
    mens1 = 'Codigo: '
    mens2 = 'Precio: '
    mens3 = 'Stock: '
    codigo = 11111
    print('Termina con un codigo = 0...')
    while (codigo > 0):
        codigo = validarNum(mens1)
        if codigo > 0: 
            if codigo not in productos:    
                descripcion = ingDescripcion()
                precio = validarNum2(mens2)
                stock = validarNum(mens3)
                productos[codigo] = [descripcion,precio,stock]
            else:
                print('Producto existente...')
    return productos
def mostrarProductos(diccProductos):
    print('Productos')
    for codigo,producto in diccProductos.items():
        print('Codigo:',codigo,' Descripcion:',producto[0],' Precio:',producto[1],' Stock:',producto[2])
def valor(mensaje):
    a = int(input(mensaje))
    return a
def mostrarIntervarlo(diccProductos):
    a = 'Desde: '
    x = valor(a)
    b = 'Hasta: '
    y = valor(b)
    cont = 0
    if x < y:
        print('Productos')
        for codigo, producto in diccProductos.items():
            if (x <= producto[2] <= y): 
                print('Codigo:',codigo,' Descripcion:',producto[0])
            else:
                cont += 1
        if cont >= 1:
            print('Hay ',cont,' productos que no pertenecen a este intervalo...')
    else:
        print('Los valores se ingresaron incorrectamente...')
def sumarStock(diccProductos):
    a = 'Suamar al stock: '
    x = valor(a) 
    b = 'Stock menor: '
    y = valor(b) 
    for codigo, producto in diccProductos.items():
        if (producto[2] < y):
            producto[2] = producto[2] + x
    mostrarProductos(diccProductos)
def verificar(diccProductos):
    pos =- 1
    for codigo, productos in diccProductos.items():
        if productos[2] == 0:
            pos = codigo
            break
    return pos
def eliminarStock(diccProductos):
    pos = 1
    while pos != -1:
        pos = verificar(diccProductos)
        if pos == -1:
            mostrarProductos(diccProductos)
        else:
            del diccProductos[pos]    
    return diccProductos
#Principal
op = 'z'
band = False
while op != 'f':
    os.system('cls')
    op = menu()
    if op == 'a':
        diccProductos = cargarProductos()
        band = True
    elif op == 'b':
        if band:
            mostrarProductos(diccProductos)
        else:
            print('Ingresar por a...')
        input('')
    elif op == 'c':
        if band:
            mostrarIntervarlo(diccProductos)
        else:
            print('Ingresar por a...')
        input('')
    elif op == 'd':
        if band:
            sumarStock(diccProductos)
        else:
            print('Ingresar por a...')
        input('')
    elif op == 'e':
        if band:
            eliminarStock(diccProductos)
        else:
            print('Ingresar por a...')
        input('')
    elif op == 'f': 
        print('Hasta luego...')